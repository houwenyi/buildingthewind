import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "mobx-react";

import "./styles/variables.less";
import "./styles/common.less";
import App from "./App";
import "antd/dist/antd.css";

import { BrowserRouter } from "react-router-dom";

import mobxStore from "./store/checkbox";
ReactDOM.render(
  <Provider {...mobxStore}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,

  document.getElementById("root")
);
