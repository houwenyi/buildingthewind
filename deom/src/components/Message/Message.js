import React, { Component } from "react";
import style from "./Message.module.less";
class Message extends Component {
  render() {
    return (
      <>
        <div className={style.container}>
          <h2 className={style.Board}>留言板</h2>
          <p>
            <strong>请勿灌水 🤖 </strong>
          </p>
          <p>
            <strong>垃圾评论过多，欢迎提供好的过滤（标记）算法</strong>
          </p>
        </div>
      </>
    );
  }
}

export default Message;
