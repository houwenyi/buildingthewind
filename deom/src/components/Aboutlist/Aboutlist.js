import React, { Component } from "react";
import style from "./Aboutlist.module.less";
import Popout from "../Popout/popout";
import { comments } from "../../api/index";
import { SmileTwoTone } from "@ant-design/icons";
import { Pagination, message } from "antd";
import Randcolors from "../Randcolors/Randcolors";
import classnames from "classnames";
import Public from "../Reuse/Reuse";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";
import { nanoid } from "nanoid";
import axios from "axios";
class Aboutlist extends Component {
  state = {
    flag: false,
    flags: false,
    minValue: 0,
    maxValue: 5,
    ind: 0,
    emoji: false,
  };
  constructor(props) {
    super(props);
    this.userName = React.createRef(null);
    this.state = {
      username: "",
      data: [],
      minValue: 0,
      maxValue: 5,
    };
  }
  componentDidMount() {
    comments().then((res) => {
      this.setState({
        data: res.data[0],
      });
    });
  }

  InputPutChange = () => {
    let val = this.userName.current.value;
    this.setState({
      username: val,
    });
  };
  edit = async () => {
    if (localStorage.getItem("token")) {
      if (!this.state.username) {
        // 这里的value就是输入的评论，这里做了非空验证
        return;
      }
      let res = await axios.post("/api/comment", {
        id: nanoid(), // nanoid实现
        name: "八维牛逼", // 姓名(用户信息)
        email: "1515948323@qq.com", // 邮箱(用户信息)
        content: this.state.username,
        html: "string",
        pass: false,
        userAgent: "Chrome 92.0.4515.159 Blink 92.0.4515.159 Windows 10",
        url: ``, // 文章url
        hostId: 1,
        parentCommentId: null,
        replyUserName: null,
        replyUserEmail: null,
        createAt: new Date().toISOString(), // 当前时间
        updateAt: new Date().toISOString(),
      });

      if (res.status === 201) {
        this.setState(
          {
            submitting: false,
          },
          () => {
            this.setState({
              username: "",
            });
            message.success("提交成功，等待系统审核");
          }
        );
      }
    } else {
      this.setState({ flag: true });
    }
  };
  // 回复
  message(ind) {
    this.setState({ ind });
  }
  // 追加回复
  editUp(index) {
    const list = [...this.state.data];
    console.log(Object.values(list[0].push()));
  }
  // 分页
  handleChange = (value) => {
    if (value <= 1) {
      this.setState({
        minValue: 0,
        maxValue: 5,
      });
    } else {
      this.setState({
        minValue: (value - 1) * 5, //每页2-1*5
        maxValue: (value - 1) * 5 + 5, //页数2-1*5+5
      });
    }
  };

  // 个人回复
  setUp() {
    this.setState({ ind: null });
  }
  choiceEmoji(emoji, e) {
    this.userName.current.value += emoji.native;
  }
  emojiFlag() {
    this.setState({
      emoji: !this.state.emoji,
    });
  }
  render() {
    const { data, minValue, maxValue, ind, emoji } = this.state;
    return (
      <>
        <Popout edit={this.state.flag} lists={this.list}></Popout>
        <div className={style.bg_color}>
          <div className={style.comment}>评论</div>
          <div className={style.js_comment_id}>
            <div className={style.Comment_box}>
              <div className={style.LeftWrapper}>
                <span className={style.essayist_name}>
                  <span>我</span>
                </span>
              </div>
              <textarea
                ref={this.userName}
                className={style.InputPut}
                onChange={this.InputPutChange}
                placeholder="请输入评论内容（支持 Markdown）"
              ></textarea>
              <div className={style.expression} style={{ position: "relative" }}>
                <SmileTwoTone
                  twoToneColor="#eb2f96"
                  onClick={() => {
                    this.emojiFlag();
                  }}
                />
                {emoji ? (
                  <Picker
                    set="apple"
                    style={{ position: "absolute", top: "80%" }}
                    onSelect={this.addEmoji}
                    onClick={(emoji, event) => {
                      this.choiceEmoji(emoji, event);
                    }}
                  />
                ) : null}
              </div>
              <button className={style.Button} onClick={() => this.edit()}>
                发布
              </button>
            </div>
            {/*评论信息 */}
            <div className={style.essayistWrapper}>
              {data &&
                data.slice(minValue, maxValue).map((item, index) => {
                  return (
                    <div key={index} className={style.essayist}>
                      <header>
                        <Randcolors item={item.name[0]} />
                        <span>
                          <strong>{item.name}</strong>
                        </span>
                      </header>
                      <main>
                        <div>
                          <p className={style.GetColor}>{item.content}</p>
                        </div>
                      </main>
                      <div className={style.Tiem}>
                        <p>
                          <span style={{ fontSize: "14px", color: "#666" }}>
                            Chrome 93.0.4577.82 Blink 93.0.4577.82 Mac OS{item.updateAt}
                          </span>
                          {/* 回复 */}
                          <span
                            onClick={() => {
                              this.message(index);
                            }}
                          >
                            <span className={style.message}>
                              <svg
                                viewBox="64 64 896 896"
                                focusable="false"
                                data-icon="message"
                                width="1em"
                                height="1em"
                                fill="currentColor"
                                aria-hidden="true"
                              >
                                <path d="M464 512a48 48 0 1096 0 48 48 0 10-96 0zm200 0a48 48 0 1096 0 48 48 0 10-96 0zm-400 0a48 48 0 1096 0 48 48 0 10-96 0zm661.2-173.6c-22.6-53.7-55-101.9-96.3-143.3a444.35 444.35 0 00-143.3-96.3C630.6 75.7 572.2 64 512 64h-2c-60.6.3-119.3 12.3-174.5 35.9a445.35 445.35 0 00-142 96.5c-40.9 41.3-73 89.3-95.2 142.8-23 55.4-34.6 114.3-34.3 174.9A449.4 449.4 0 00112 714v152a46 46 0 0046 46h152.1A449.4 449.4 0 00510 960h2.1c59.9 0 118-11.6 172.7-34.3a444.48 444.48 0 00142.8-95.2c41.3-40.9 73.8-88.7 96.5-142 23.6-55.2 35.6-113.9 35.9-174.5.3-60.9-11.5-120-34.8-175.6zm-151.1 438C704 845.8 611 884 512 884h-1.7c-60.3-.3-120.2-15.3-173.1-43.5l-8.4-4.5H188V695.2l-4.5-8.4C155.3 633.9 140.3 574 140 513.7c-.4-99.7 37.7-193.3 107.6-263.8 69.8-70.5 163.1-109.5 262.8-109.9h1.7c50 0 98.5 9.7 144.2 28.9 44.6 18.7 84.6 45.6 119 80 34.3 34.3 61.3 74.4 80 119 19.4 46.2 29.1 95.2 28.9 145.8-.6 99.6-39.7 192.9-110.1 262.7z"></path>
                              </svg>
                            </span>
                            回复
                          </span>
                        </p>
                        <div
                          className={classnames(
                            item.id,
                            `${ind === index ? style.responses : style.response}`
                          )}
                        >
                          <Randcolors></Randcolors>
                          <textarea
                            className={style.InputPuts}
                            placeholder={`${"回复" + item.name}`}
                          ></textarea>
                          <div className={style.expression}>
                            <SmileTwoTone twoToneColor="#eb2f96" />
                          </div>
                          {/* 追加评论 */}
                          <button
                            disabled
                            className={style.Button}
                            onClick={() => this.editUp(index)}
                          >
                            发布
                          </button>
                          <button className={style.Button} onClick={() => this.setUp()}>
                            收起
                          </button>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
            {/* 分页 */}
            <div className={style.paging}>
              <Pagination
                defaultCurrent={1} // 默认在第一个页面
                defaultPageSize={5} // 默认一个页面显示3个数据
                // 点击页面触发更新，点击时，默认传入页面值，例如第一页，值为1
                onChange={this.handleChange}
                // 设置整个数据的总数量
                total={data.length}
              />
            </div>
            {/* <div className={style.public}>
              <h2 className={style.Study}>推荐阅读</h2>
              <Public />
            </div> */}
          </div>
          <div className={style.public}>
            <h2 className={style.Study}>推荐阅读</h2>
            <div className={style.publics}>
              <Public />
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Aboutlist;
