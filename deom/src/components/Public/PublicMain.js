import React, { Component } from "react";
import Archives from "../Archives/Archives";
import ArticleLeft from "../../components/ArticleComs/ArticleLeft";
import style from "./Public.module.less";
import { withRouter } from "react-router-dom";
import "./Public.css";
import PublicRight from "./PublicRight";
import { HeartOutlined, EyeOutlined, ShareAltOutlined } from "@ant-design/icons";
import moment from "@/api/Moment";
@withRouter
class PublicMain extends Component {
  goDetail(id) {
    this.props.history.push(`/home/Detail/${id}`);
  }
  render() {
    console.log(this.props, "123");
    return (
      <>
        {this.props.location.pathname === "/home/archives" ? (
          <div className="Bbox">
            <div className="Bbox-Left">
              <div className="container">
                <Archives></Archives>
              </div>
            </div>
            <div className="Bbox-Right">
              <PublicRight Right={this.props.Right} Left={this.props.Left} />
            </div>
          </div>
        ) : (
          <div className={style.knowBox}>
            {
              <div className={style.knowBox}>
                <div className={style.konwBoxMid}>
                  {this.props.location.pathname === "/home/knowledge" ||
                  this.props.location.pathname === "/home/boards" ||
                  this.props.location.pathname === "/home/about" ? null : (
                    <ArticleLeft {...this.props}></ArticleLeft>
                  )}
                </div>
              </div>
            }
          </div>
        )}
      </>
    );
  }
}

export default PublicMain;
