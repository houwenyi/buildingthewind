import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { FormattedMessage } from "react-intl";

import style from "./Header.module.less";
import Logo from "../../public/images/wipi-logo.png";
import { MenuOutlined } from "@ant-design/icons";

import Login from "../Login";
import Search from "../Search";

@inject("titleStore")
@observer
class Header extends Component {
  state = {
    isflage: true,
    flag: false,
    loginFlag: false,
    langFlag: false,
  };
  setIsflage = () => {
    console.log(1);
    this.setState(
      {
        isflage: !this.state.isflage,
      },
      () => {
        document.querySelector("body").classList.toggle("dark");
      }
    );
  };

  changeFlag() {
    this.setState({
      flag: !this.state.flag,
    });
  }

  changeLoginFlag() {
    this.setState({
      loginFlag: !this.state.loginFlag,
    });
  }
  changelangFlag() {
    this.setState({
      langFlag: !this.state.langFlag,
    });
  }
  render() {
    let { isflage, flag, loginFlag, langFlag } = this.state;
    return (
      <div id="Header" className={style.Header}>
        <div id={style.Header_logo}>
          <a href="/">
            <img src={Logo} alt="logo"></img>
          </a>
        </div>
        <div id={style.Header_icon}>
          <MenuOutlined style={{ color: "#ff0064", fontSize: "24px" }} />
        </div>
        <div id={style.Header_router}>
          <NavLink to="/home/articles/all">
            <FormattedMessage id="articles" />
          </NavLink>
          <NavLink to="/home/archives">
            <FormattedMessage id="archives" />
          </NavLink>
          <NavLink to="/home/knowledge">
            <FormattedMessage id="knowledge" />
          </NavLink>
          <NavLink to="/home/boards">
            <FormattedMessage id="board" />
          </NavLink>
          <NavLink to="/home/about">
            <FormattedMessage id="about" />
          </NavLink>
        </div>
        <div id={style.Header_btn}>
          <div
            className={style.HeaderBtn_left}
            onMouseEnter={() => this.changelangFlag()}
            onMouseLeave={() => this.changelangFlag()}
          >
            <svg
              viewBox="0 0 1024 1024"
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              width="1em"
              height="1em"
              style={{ width: "24px", height: "24px" }}
            >
              <path
                d="M547.797333 638.208l-104.405333-103.168 1.237333-1.28a720.170667 720.170667 0 0 0 152.490667-268.373333h120.448V183.082667h-287.744V100.906667H347.605333v82.218666H59.818667V265.386667h459.178666a648.234667 648.234667 0 0 1-130.304 219.946666 643.242667 643.242667 0 0 1-94.976-137.728H211.541333a722.048 722.048 0 0 0 122.453334 187.434667l-209.194667 206.378667 58.368 58.368 205.525333-205.525334 127.872 127.829334 31.232-83.84m231.424-208.426667h-82.218666l-184.96 493.312h82.218666l46.037334-123.306667h195.242666l46.464 123.306667h82.218667l-185.002667-493.312m-107.690666 287.744l66.56-178.005333 66.602666 178.005333z"
                fill="currentColor"
              ></path>
            </svg>
            <div
              className={style.HeaderBtn_leftMask}
              style={{ display: langFlag ? "block" : "none" }}
            >
              <p onClick={() => this.props.titleStore.changeLange("en-US")}>
                <FormattedMessage id="english" />
              </p>
              <p onClick={() => this.props.titleStore.changeLange("zh-CN")}>
                <FormattedMessage id="chinese" />
              </p>
            </div>
          </div>

          <div className={style.HaderBtn_center} onClick={() => this.setIsflage()}>
            {isflage ? (
              <svg viewBox="0 0 24 24" style={{ width: "24px", height: "24px" }}>
                <path
                  d="M3.55,18.54L4.96,19.95L6.76,18.16L5.34,16.74M11,22.45C11.32,22.45 13,22.45 13,22.45V19.5H11M12,5.5A6,6 0 0,0 6,11.5A6,6 0 0,0 12,17.5A6,6 0 0,0 18,11.5C18,8.18 15.31,5.5 12,5.5M20,12.5H23V10.5H20M17.24,18.16L19.04,19.95L20.45,18.54L18.66,16.74M20.45,4.46L19.04,3.05L17.24,4.84L18.66,6.26M13,0.55H11V3.5H13M4,10.5H1V12.5H4M6.76,4.84L4.96,3.05L3.55,4.46L5.34,6.26L6.76,4.84Z"
                  fill="currentColor"
                ></path>
              </svg>
            ) : (
              <svg viewBox="0 0 24 24" style={{ width: "24px", height: "24px" }}>
                <path
                  fill="currentColor"
                  d="M17.75,4.09L15.22,6.03L16.13,9.09L13.5,7.28L10.87,9.09L11.78,6.03L9.25,4.09L12.44,4L13.5,1L14.56,4L17.75,4.09M21.25,11L19.61,12.25L20.2,14.23L18.5,13.06L16.8,14.23L17.39,12.25L15.75,11L17.81,10.95L18.5,9L19.19,10.95L21.25,11M18.97,15.95C19.8,15.87 20.69,17.05 20.16,17.8C19.84,18.25 19.5,18.67 19.08,19.07C15.17,23 8.84,23 4.94,19.07C1.03,15.17 1.03,8.83 4.94,4.93C5.34,4.53 5.76,4.17 6.21,3.85C6.96,3.32 8.14,4.21 8.06,5.04C7.79,7.9 8.75,10.87 10.95,13.06C13.14,15.26 16.1,16.22 18.97,15.95M17.33,17.97C14.5,17.81 11.7,16.64 9.53,14.5C7.36,12.31 6.2,9.5 6.04,6.68C3.23,9.82 3.34,14.64 6.35,17.66C9.37,20.67 14.19,20.78 17.33,17.97Z"
                ></path>
              </svg>
            )}
          </div>

          <div className={style.HeaderBtn_right} onClick={() => this.changeFlag()}>
            <svg
              viewBox="64 64 896 896"
              focusable="false"
              data-icon="search"
              width="1em"
              height="1em"
              fill="currentColor"
              aria-hidden="true"
              style={{ width: "24px", height: "24px" }}
            >
              <path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
            </svg>
          </div>

          <div className={style.loginBtn}>
            <button onClick={() => this.changeLoginFlag()}>
              {/* <FormattedMessage id="login" /> */}
              {localStorage.getItem("token") ? "已登录" : "登录"}
            </button>
          </div>
        </div>
        <Search flag={flag} changeFlag={() => this.changeFlag()} />
        <Login flag={loginFlag} changeFlag={() => this.changeLoginFlag()} />
      </div>
    );
  }
}

export default Header;
