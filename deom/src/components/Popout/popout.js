import React, { useState, useEffect, useReducer, useRef } from "react";
import { Modal, Input, message } from "antd";
import "./popout.css";
export default function Popout(props) {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [state, setstate] = useState(false);
  const reducer = useReducer(
    (state, action) => {
      switch (action.type) {
        case "app":
          break;

        default:
          return {
            ...state,
          };
      }
    },
    { isModalVisible, state }
  );

  const names = useRef(null);
  const mail = useRef(null);
  useEffect(() => {
    setstate(props.edit);
    setIsModalVisible(props.edit);
  }, [props]);
  const handleOk = () => {
    setIsModalVisible(false);
    const reg = /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(.[a-zA-Z0-9_-]+)+$/;
    if (names.current.state.value === "侯文艺" && reg.test(mail.current.state.value)) {
      message.success("提交成功");
      localStorage.setItem("token", 666);
    } else {
      message.error(" 提交失败");
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <div>
      {reducer.state}
      <Modal
        title="请设置你的信息"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        cancelText="取消"
        okText="提交"
      >
        <p>
          <b>*</b> <span>名称:</span> <Input placeholder="用户名" ref={names} />
        </p>
        <br />
        <p>
          <b>*</b> <span>邮箱:</span> <Input placeholder="邮箱" ref={mail} />
        </p>
      </Modal>
    </div>
  );
}
