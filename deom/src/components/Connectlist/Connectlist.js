import React, { Component } from "react";
import Aboutlist from "../Aboutlist/Aboutlist";
import Message from "../Message/Message";
class Connectlist extends Component {
  render() {
    return (
      <>
        {/* 头部 */}
        <Message />
        {/* 列表 */}
        <Aboutlist />
      </>
    );
  }
}

export default Connectlist;
