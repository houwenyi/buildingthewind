import React, { Component } from "react";
import { FileList } from "../../api/index";
import QueueAnim from "rc-queue-anim";
import { withRouter } from "react-router-dom";
import moment from "moment";
@withRouter
class Archives extends Component {
  state = {
    List: [],
  };
  componentDidMount() {
    FileList().then((res) => {
      // console.log(res.data);
      this.setState({
        List: res.data,
      });
    });
  }
  render() {
    let { List } = this.state;
    return (
      <div className="_3tC3y9W6nEYVdAwGypNta9">
        <section className="_1r_Dp71aY9wU_PZOrRyGzl">
          <div className="_31uY1VinxiwKyG_XZXFpv">
            <div className="_1tJ6zWK6o_q8KafBWysrmZ">
              <p>
                <span>归档</span>
              </p>
              <p>
                共计 <span>{List ? List.length * 3 : 0}</span> 篇
              </p>
            </div>
            <div className="GIf9xHwpzxlxHcDyE8sCn">
              <h2>2021</h2>
              <div className="_172XrbegThYDU7WfEHTUpM">
                <h3>July </h3>{" "}
                <QueueAnim className="queue-simple" delay={400} type="left">
                  {List &&
                    List.map((item, index) => {
                      return (
                        <ul key={index}>
                          <li
                            onClick={() => {
                              this.props.history.push({
                                pathname: `/home/Detail/${item.id}`,
                              });
                            }}
                          >
                            <span className="OTAyqb_MWljCg91UnBG5u">
                              {" "}
                              {moment(item.createAt)
                                .format("L")
                                .split("/")
                                .join("-")
                                .split("-2021")}
                            </span>
                            <span className="JxUmPoeJKqnfS58GvD3vz">{item.title}</span>
                          </li>
                        </ul>
                      );
                    })}{" "}
                </QueueAnim>
              </div>
            </div>

            <div className="GIf9xHwpzxlxHcDyE8sCn">
              <h2>2020</h2>
              <div className="_172XrbegThYDU7WfEHTUpM">
                <h3>May</h3>{" "}
                <QueueAnim className="queue-simple" delay={400} type="left">
                  {List &&
                    List.map((item, index) => {
                      return (
                        <ul key={index}>
                          <li
                            onClick={() => {
                              this.props.history.push({
                                pathname: `/Detail/${item.id}`,
                              });
                            }}
                          >
                            <span className="OTAyqb_MWljCg91UnBG5u">
                              {" "}
                              {moment(item.createAt)
                                .format("L")
                                .split("/")
                                .join("-")
                                .split("-2021")}
                            </span>
                            <span className="JxUmPoeJKqnfS58GvD3vz">{item.title}</span>
                          </li>
                        </ul>
                      );
                    })}{" "}
                </QueueAnim>
              </div>
            </div>

            <div className="GIf9xHwpzxlxHcDyE8sCn">
              <h2>2019</h2>
              <div className="_172XrbegThYDU7WfEHTUpM">
                <h3>October</h3>{" "}
                <QueueAnim className="queue-simple" delay={400} type="left">
                  {List &&
                    List.map((item, index) => {
                      return (
                        <ul key={index}>
                          <li
                            onClick={() => {
                              this.props.history.push({
                                pathname: `/archivesDetail/${item.id}`,
                              });
                            }}
                          >
                            <span className="OTAyqb_MWljCg91UnBG5u">
                              {" "}
                              {moment(item.createAt)
                                .format("L")
                                .split("/")
                                .join("-")
                                .split("-2021")}
                            </span>
                            <span className="JxUmPoeJKqnfS58GvD3vz">{item.title}</span>
                          </li>
                        </ul>
                      );
                    })}{" "}
                </QueueAnim>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Archives;
