import React, { Component } from "react";
import style from "./Publish.module.less";
import { SmileTwoTone } from "@ant-design/icons";
class Publish extends Component {
  render() {
    return (
      <>
        <div className={style.LeftWrapper}>
          <span className={style.essayist_name}>
            <span>我</span>
          </span>
        </div>
        <textarea
          ref="username"
          className={style.InputPut}
          placeholder="请输入评论内容（支持 Markdown）"
        ></textarea>
        <div className={style.expression}>
          <SmileTwoTone twoToneColor="#eb2f96" />
        </div>
        <button className={style.Button}>发布</button>
      </>
    );
  }
}

export default Publish;
