import React, { Component } from "react";
import style from "./Randcolors.module.less";
class Randcolors extends Component {
  rendomColor() {
    this.r = Math.floor(Math.random() * 255);
    this.g = Math.floor(Math.random() * 255);
    this.b = Math.floor(Math.random() * 255);
    this.color = "rgba(" + this.r + "," + this.g + "," + this.b + ",0.8)";
    return this.color;
  }
  render() {
    return (
      <span className={style.essayist_name} style={{ backgroundColor: this.rendomColor() }}>
        <span> {this.props.item}</span>
      </span>
    );
  }
}

export default Randcolors;
