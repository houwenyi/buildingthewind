import React, { Component } from "react";
import Archives from "../Archives/Archives";
import ArticleLeft from "../ArticleComs/ArticleLeft";
import style from "./Reuse.module.less";
import { withRouter } from "react-router-dom";
import "./Reuse.css";
import { HeartOutlined, EyeOutlined, ShareAltOutlined } from "@ant-design/icons";
import moment from "@/api/Moment";
@withRouter
class PublicMain extends Component {
  goDetail(id) {
    this.props.history.push(`/home/Detail/${id}`);
  }
  render() {
    return (
      <>
        {this.props.location.pathname === "/home/archives" ? (
          <>
            <Archives></Archives>
          </>
        ) : (
          <div className={style.knowBox}>
            {
              <div className={style.knowBox}>
                <div className={style.konwBoxMid}>
                  {this.props.location.pathname === "/home/knowledge" ||
                  this.props.location.pathname === "/home/boards" ||
                  this.props.location.pathname === "/home/about" ? null : (
                    <ArticleLeft {...this.props}></ArticleLeft>
                  )}
                  {this.props.data.length > 0 &&
                    this.props.data.map((item) => {
                      return (
                        <div
                          key={item.id}
                          className={style.knowMain}
                          onClick={() => {
                            this.goDetail(item.id);
                          }}
                        >
                          <div className={style.knowMainLeft}>
                            <h3>
                              <span>{item.title}</span>&emsp;
                              <span style={{ fontSize: "16px" }}>
                                {moment(item.createAt).startOf("天").fromNow()}
                              </span>
                            </h3>
                            <p>{item.summary !== null ? item.summary : item.title}</p>
                            <div className={style.knowMain_Icon}>
                              <p>
                                <HeartOutlined />
                                <span>{item.likes}</span>
                              </p>
                              <p>
                                <EyeOutlined />
                                <span>{item.likes}</span>
                              </p>
                              <p>
                                <ShareAltOutlined />
                                <span>分享</span>
                              </p>
                            </div>
                          </div>
                          {item.cover === null ? null : (
                            <div className={style.knowMainRight}>
                              <img src={item.cover} alt="" />
                            </div>
                          )}
                        </div>
                      );
                    })}
                </div>
              </div>
            }
          </div>
        )}
      </>
    );
  }
}

export default PublicMain;
