import React, { Component } from "react";
import { Spin, Alert } from "antd";
import QueueAnim from "rc-queue-anim";
import { inject, observer } from "mobx-react";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router-dom";
import moment from "@/api/Moment";
@inject("titleStore")
@observer
@withRouter
class PublicRight extends Component {
  goDetail(id) {
    this.props.history.push(`/home/detail/${id}`);
  }
  render() {
    console.log(this.props, "right");
    return (
      <div className="rightBox">
        <div className="Bbox-R-top">
          <h4>
            <FormattedMessage id="Recommend" />
          </h4>
          {
            <ul>
              <QueueAnim className="queue-simple" delay={400} type="left">
                {this.props.Right && this.props.Right.length > 0 ? (
                  this.props.Right.map((item, index) => {
                    return (
                      <li
                        style={{ top: index * 37 }}
                        key={item.id}
                        onClick={() => {
                          this.props.history.push({
                            pathname: `/home/Detail/${item.id}`,
                          });
                        }}
                      >
                        <p>{item.title}</p>
                        <span className="time">{moment(item.createAt).startOf().fromNow()}</span>
                      </li>
                    );
                  })
                ) : (
                  <Spin tip="Loading...">
                    <Alert type="info" />
                  </Spin>
                )}
              </QueueAnim>
            </ul>
          }
        </div>
        {/* <Affix offsetTop={5} onChange={(affixed) => console.log(affixed)}> */}
        <div className="Bbox-R-bottom">
          <h4>
            <FormattedMessage id="Category" />
          </h4>
          {
            <ul>
              <QueueAnim className="queue-simple" delay={400} type="left">
                {this.props.Left && this.props.Left.length > 0 ? (
                  this.props.Left.map((item, index) => {
                    return (
                      index > 6 && (
                        <li style={{ top: index * 37 }} key={item.id}>
                          <p>{item.label}</p>
                          <span>共计 {item.articleCount} 篇文章</span>
                        </li>
                      )
                    );
                  })
                ) : (
                  <Spin tip="Loading...">
                    <Alert type="info" />
                  </Spin>
                )}
              </QueueAnim>
            </ul>
          }
        </div>
        {/* </Affix> */}
      </div>
    );
  }
}

export default PublicRight;
