import React, { Component } from "react";
import "./Reuse.css";
import { withRouter } from "react-router-dom";
import PublicRight from "./ReuseRight";
import PublicMain from "./ReuseMain";
import { knowImgs, FileLeft, FileRight, getAllData } from "@/api";
import Data from "@/mock/Data.json";
@withRouter
class Public extends Component {
  state = {
    data: [],
    allList: [],
    allData: [],
    rightList: [],
    archivesRight: [],
    testListData: [],
  };
  async componentDidMount() {
    let knowImgData = await knowImgs();
    let result = await FileLeft();
    let archivesRightData = await FileRight();
    let allDatas = await getAllData();
    console.log(allDatas);
    this.setState({
      data: knowImgData.data[0],
      allData: result.data,
      archivesRight: archivesRightData.data,
      allList: allDatas.data,
      // testListData
    });
  }

  render() {
    const { data, allData, archivesRight, allList } = this.state;
    console.log(data, Data);
    return (
      <div
        className={
          this.props.location.pathname === "/home/boards" ||
          this.props.location.pathname === "/home/about"
            ? "DevBox"
            : "Bbox"
        }
      >
        <div
          className={
            this.props.location.pathname === "/home/boards" ||
            this.props.location.pathname === "/home/about"
              ? "DevLeft"
              : "Bbox-Left"
          }
        >
          <div className="container">
            <PublicMain
              {...this.props}
              data={this.props.location.pathname === "/home/knowledge" ? data : allList}
              Right={archivesRight}
              Left={allData}
            ></PublicMain>
          </div>
        </div>
        {this.props.location.pathname === "/home/boards" ||
        this.props.location.pathname === "/home/about" ? null : (
          <div className="Bbox-Right">
            <PublicRight Right={archivesRight} Left={allData} />
          </div>
        )}
      </div>
    );
  }
}

export default Public;
