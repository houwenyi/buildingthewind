import React, { useState } from "react";
import { Affix } from "antd";

const AffixBox = (props) => {
  const [top] = useState(5);
  return (
    <div className="affixwrap">
      <Affix offsetTop={top}>{props.children}</Affix>
    </div>
  );
};
export default AffixBox;
