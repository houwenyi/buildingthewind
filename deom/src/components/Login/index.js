import React, { useRef } from "react";
import style from "./index.module.less";

const Index = ({ flag, changeFlag }) => {
  const userName = useRef(null);
  const password = useRef(null);
  function islogin() {
    if (userName.current.value === "123@163.com" && password.current.value === "123") {
      changeFlag();
      localStorage.setItem("token", "666");
    }
  }
  function exitLogin() {
    changeFlag();
    localStorage.removeItem("token");
  }
  return (
    <div
      className={style.Login_Mask}
      style={{ display: flag ? "flex" : "none" }}
      onClick={(e) => {
        if (e.target.className === style.Login_Mask) {
          changeFlag();
        }
      }}
    >
      <div className={style.Login_Text}>
        <div className={style.LoginText_head}>
          <h3>账密登录</h3>
          <span onClick={() => changeFlag()}>X</span>
          {console.log(flag)}
        </div>
        {localStorage.getItem("token") ? (
          <div>
            <h1 style={{ textAlign: "center" }}>您已登录</h1>
            <p style={{ textAlign: "center" }}>
              <button
                onClick={() => exitLogin()}
                style={{
                  color: "#fff",
                  background: "skyblue",
                  border: "1px solid #ccc",
                  cursor: "pointer",
                  padding: "5px",
                }}
              >
                点击退出登录
              </button>
            </p>
          </div>
        ) : (
          <div className={style.LoginText_body}>
            <input type="text" placeholder="邮箱" ref={userName}></input>
            <input type="text" placeholder="密码" ref={password}></input>
            <button onClick={() => islogin()}>登录</button>
          </div>
        )}
      </div>
    </div>
  );
};

export default Index;
