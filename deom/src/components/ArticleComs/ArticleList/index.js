import React, { Component } from "react";
import { HeartOutlined, EyeOutlined, ShareAltOutlined } from "@ant-design/icons";

import style from "./index.module.less";
import moment from "@/api/Moment";

export default class index extends Component {
  goDetail(id) {
    this.props.history.push(`/home/detail/${id}`);
  }

  render() {
    const { allData } = this.props;
    return (
      <div>
        {allData &&
          allData.map((item) => {
            return (
              <div
                key={item.id}
                className={style.knowMain}
                onClick={() => {
                  this.goDetail(item.id);
                }}
              >
                <div className={style.knowMainLeft}>
                  <h3>
                    {item.title}
                    <span>{moment(item.createAt).startOf("天").fromNow()}</span>
                  </h3>
                  <p>{item.title}</p>
                  <div className={style.knowMain_Icon}>
                    <p>
                      <HeartOutlined />
                      <span>{item.likes}</span>
                    </p>
                    <p>
                      <EyeOutlined />
                      <span>{item.likes}</span>
                    </p>
                    <p>
                      <ShareAltOutlined />
                      <span>{item.likes}</span>
                    </p>
                  </div>
                </div>
                <div className={style.knowMainRight}>
                  <img src={item.cover} alt="" />
                </div>
              </div>
            );
          })}
      </div>
    );
  }
}
