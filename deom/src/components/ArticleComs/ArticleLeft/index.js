import React, { Component } from "react";
import style from "./index.module.less";
import { NavLink } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { FormattedMessage } from "react-intl";

import Swiper from "../Swiper";
import ArticleLeftTop from "../ArticleLeftTop";
import RouterView from "@/router/RouterView";
import Back from "../../../components/Back/Back";
@inject("titleStore")
@observer
class ArticleLeft extends Component {
  state = {
    index: 0,
    navList: [
      {
        lang: "all",
        tit: "All",
        path: "/home/articles/all",
      },
      {
        lang: "frontend",
        tit: "前端",
        path: "/home/articles/frontend",
      },
      {
        lang: "affterend",
        tit: "后端",
        path: "/home/articles/affterend",
      },
      {
        lang: "read",
        tit: "阅读",
        path: "/home/articles/read",
      },
      {
        lang: "linux",
        tit: "Linux",
        path: "/home/articles/linux",
      },
      {
        lang: "lettcode",
        tit: "Lettcode",
        path: "/home/articles/lettcode",
      },
      {
        lang: "hightlights",
        tit: "要闻",
        path: "/home/articles/hightlights",
      },
    ],
  };

  changeIndex(index) {
    this.setState({
      index: index,
    });
  }

  render() {
    const { navList } = this.state;
    return (
      <div className={style.konwBoxMid}>
        <div className={style.konwBoxMid_head}>
          {this.props.location.pathname === "/home/articles/all" ? (
            <Swiper />
          ) : (
            navList.map((item, index) => {
              if (this.props.location.pathname === item.path) {
                return <ArticleLeftTop key={index} text={item.tit} />;
              } else {
                return "";
              }
            })
          )}
        </div>
        <div className={style.leftBtm}>
          <div className={style.konwBoxMid_router}>
            {navList &&
              navList.map((item, index) => {
                return (
                  <NavLink key={index} to={item.path} onClick={() => this.changeIndex(index)}>
                    <FormattedMessage id={item.lang} />
                  </NavLink>
                );
              })}
          </div>
          <div className={style.konwBoxMid_view}>
            <RouterView routerList={this.props.routerList} />
          </div>
        </div>
        <Back></Back>
      </div>
    );
  }
}

export default ArticleLeft;
