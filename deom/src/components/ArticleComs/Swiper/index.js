import React, { Component } from "react";
import style from "./index.module.less";
import { Carousel } from "antd";

import moment from "../../../api/Moment";
import SwiperFirst from "../../../public/images/Swiper_first.png";
import SwiperSecond from "../../../public/images/Swiper_second.webp";
import SwiperThird from "../../../public/images/Swiper_third.jpeg";

export default class Swiper extends Component {
  state = {
    swiperList: [
      {
        img: SwiperFirst,
        createAt: "2021-10-14T12:07:36.680Z",
        title: "哈哈哈哈123",
      },
      {
        img: SwiperSecond,
        createAt: "2021-10-18T12:07:36.680Z",
        title: "新建21",
      },
      {
        img: SwiperThird,
        createAt: "2021-10-10T12:07:36.680Z",
        title: "在线征婚",
      },
    ],
  };

  render() {
    const { swiperList } = this.state;
    return (
      <div className={style.Swiper}>
        <Carousel autoplay adaptiveHeight useCSS className={style.SwiperText}>
          {swiperList &&
            swiperList.map((item, index) => {
              return (
                <div key={index} className={style.Swiper_item}>
                  <img src={item.img} alt="404" style={{ width: "100%", height: "340px" }}></img>
                  <div className={style.SwiperMask}>
                    <p>
                      <b>{item.title}</b>
                      <span>{moment(item.createAt).startOf("天").fromNow()}</span>
                    </p>
                  </div>
                </div>
              );
            })}
        </Carousel>
      </div>
    );
  }
}
