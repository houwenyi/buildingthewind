import React, { Component } from "react";
import style from "./index.module.less";
import { inject, observer } from "mobx-react";
import { FormattedMessage } from "react-intl";
@inject("titleStore")
@observer
class ArticleLeftTop extends Component {
  render() {
    const { text } = this.props;
    return (
      <div id={style.ArticleTop}>
        <div id={style.TopCenter}>
          <p>
            <span>{text}</span>
            <FormattedMessage id="Category" />
          </p>
          <p>
            <FormattedMessage id="Totally" />
            <span>{Math.floor(Math.random() * 15)}</span>
            <FormattedMessage id="Count" />
          </p>
        </div>
      </div>
    );
  }
}

export default ArticleLeftTop;
