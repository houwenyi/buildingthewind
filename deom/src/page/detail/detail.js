import React, { Component } from "react";
import QRCode from "qrcode.react";
import moment from "@/api/Moment";
import QueueAnim from "rc-queue-anim";
import { Anchor, Modal } from "antd";
import ArticleComment from "../../components/ArticleComment/ArticleComment";
import "antd/dist/antd.css";
import { FileRight, knowImgs, getFileDetail } from "../../api";
import "./Detail.css";
const data = [
  { href: "#API", title: "什么是Markdown 编辑器" },
  { href: "#AnchorProp-s", title: "1.待办事宜Todo 列表" },
  { href: "#Link-Props", title: "2. 高亮一段代码[^code]" },
  { href: "#Link-Prop2", title: "3.绘制表格" },
  {
    href: "#Link-Prop1",
    title: "4.嵌入网址",
  },
];
export default class componentName extends Component {
  state = {
    list: [],
    key: [],
    index: 2,
    flags: false,
    Right: [],
    visible: false,
    choiceArr: [],
    ind: 0,
  };
  constructor(props) {
    super(props);
    this.LeftSroll = React.createRef();
    this.Top = React.createRef("");
  }
  componentDidMount() {
    const { id } = this.props.match.params;
    document.querySelector("#Home").addEventListener("scroll", () => {
      console.log(1);
    });
    knowImgs(id).then((res) => {
      let newArr = res.data[0].filter((item) => item.id === id);
      if (newArr.length > 0) {
        this.setState({
          list: newArr[0],
        });
      } else {
        getFileDetail(id).then((res) => {
          this.setState({ list: res.data, index: res.data.likes });
        });
      }
    });
    FileRight().then((res) => {
      this.setState({
        Right: res.data,
      });
    });
  }
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = () => {
    this.setState({
      visible: false,
    });
  };
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  ChangeId(id) {
    this.props.history.push({
      pathname: `/home/Detail/${id}`,
    });
    this.props.history.go(0);
  }
  setLike() {
    this.setState({
      flags: !this.state.flags,
      index: this.state.flags ? this.state.index - 1 : this.state.index + 1,
    });
  }

  //点击事件
  CreateIndex = (index) => {
    this.setState({ ind: index });

    this.LeftSroll.current.scrollTop =
      Array.from(document.querySelectorAll("h3"))[index].offsetTop - 100;
  };
  // 楼层

  onScollTop = () => {
    var Hs = ["h1", "h2", "h3", "h4", "h5", "h6"];
    data = [...this.LeftSroll.current.children[1].children].filter((item) =>
      Hs.includes(item.nodeName.toLowerCase())
    );
    console.log(data);
    Array.from(document.querySelectorAll("h3")).forEach((item, index) => {
      if (this.LeftSroll.current.scrollTop >= item.offsetTop - 100) {
        this.setState({ ind: index });
      }
    });
  };
  render() {
    const { list, Right, index } = this.state;

    return (
      <div className="fileDetail" ref={this.Top}>
        <div className="file-left">
          <div className="_3DPqg1sEeVKyZvQGiGR7MK">
            <div className="opW4K-r7eKwtlgiMzfCHm">
              <span className="ant-badge">
                <div className="_1qU35tL8S-yk6hGSLgkF1n">
                  <svg
                    viewBox="0 0 1024 1024"
                    version="1.1"
                    xmlns="http://www.w3.org/2000/svg"
                    width="1em"
                    height="1em"
                    onClick={() => {
                      this.setLike();
                    }}
                    className={this.state.flags ? "active" : ""}
                  >
                    <path
                      d="M859.8 191.2c-80.8-84.2-212-84.2-292.8 0L512 248.2l-55-57.2c-81-84.2-212-84.2-292.8 0-91 94.6-91 248.2 0 342.8L512 896l347.8-362C950.8 439.4 950.8 285.8 859.8 191.2z"
                      fill="currentColor"
                    ></path>
                  </svg>
                </div>
                {index ? (
                  <sup
                    data-show="false"
                    className="ant-scroll-number ant-badge-count ant-badge-count-sm"
                    title="2"
                  >
                    <span className="ant-scroll-number-only">
                      <p className="ant-scroll-number-only-unit current">{index}</p>
                    </span>
                  </sup>
                ) : (
                  ""
                )}
              </span>
            </div>

            <div className="opW4K-r7eKwtlgiMzfCHm">
              <a href="#components-anchor-demo-basic">
                <div className="oymssHUh0CsJHhRBTfMNy">
                  <svg
                    viewBox="0 0 1024 1024"
                    version="1.1"
                    xmlns="http://www.w3.org/2000/svg"
                    width="1em"
                    height="1em"
                  >
                    <path
                      d="M988.8 512a348.8 348.8 0 0 1-144.96 278.72v208.32l-187.84-131.52a387.2 387.2 0 0 1-56 4.8A408.64 408.64 0 0 1 384 811.84l-36.8-23.04a493.76 493.76 0 0 0 52.8 3.2 493.44 493.44 0 0 0 51.2-2.88c221.44-23.04 394.24-192 394.24-400a365.12 365.12 0 0 0-4.16-51.84 373.44 373.44 0 0 0-48.96-138.56l18.88 11.2A353.6 353.6 0 0 1 988.8 512z m-198.72-128c0-192-169.6-349.76-378.24-349.76h-24C192 47.04 33.92 198.72 33.92 384a334.08 334.08 0 0 0 118.4 253.12v187.52l86.08-60.48 66.24-46.4a396.16 396.16 0 0 0 107.52 16C620.48 734.08 790.08 576 790.08 384z"
                      fill="currentColor"
                    ></path>
                  </svg>
                </div>
              </a>
            </div>
            <div className="opW4K-r7eKwtlgiMzfCHm">
              <span onClick={this.showModal.bind(this)}>
                <div className="_2pkVHcLoLMRrby9QPbVsmv">
                  <svg
                    viewBox="0 0 1024 1024"
                    version="1.1"
                    xmlns="http://www.w3.org/2000/svg"
                    width="1em"
                    height="1em"
                  >
                    <path
                      d="M753.607 584.7c-48.519 0-91.596 23.298-118.66 59.315l-233.123-116.96c3.684-12.936 5.657-26.591 5.657-40.71 0-15.465-2.369-30.374-6.76-44.391l232.241-116.52c26.916 37.549 70.919 62.017 120.644 62.017 81.926 0 148.34-66.412 148.34-148.34 0-81.926-66.413-148.34-148.34-148.34-81.927 0-148.34 66.413-148.34 148.34 0 5.668 0.33 11.258 0.948 16.762l-244.945 122.892c-26.598-25.259-62.553-40.762-102.129-40.762-81.926 0-148.34 66.412-148.34 148.34s66.413 148.34 148.34 148.34c41.018 0 78.144-16.648 104.997-43.555l242.509 121.668c-0.904 6.621-1.382 13.374-1.382 20.242 0 81.927 66.412 148.34 148.34 148.34s148.34-66.413 148.34-148.34c-0.001-81.925-66.409-148.339-148.336-148.339l0 0z"
                      fill="currentColor"
                    ></path>
                  </svg>
                </div>
              </span>
            </div>
          </div>
        </div>
        <div className="file-center" onScroll={() => this.onScollTop()} ref={this.LeftSroll}>
          <div className="file-c-t" ref={this.LeftSroll}>
            {list.cover ? <img src={list.cover} alt="" /> : ""}
            <h1>{list.title}</h1>
            <em>
              发布于{list.publishAt} <b>·</b> 阅读量{list.views}
            </em>
          </div>
          <div className="file-c-main" dangerouslySetInnerHTML={{ __html: list.html }}></div>
          <div className="file-c-b">
            <ArticleComment id={list.id} />
          </div>
        </div>
        <div className="file-right">
          <div className="file-r-t">
            <h4>文章分类</h4>
            {
              <ul>
                <QueueAnim className="queue-simple" delay={400} type="left">
                  {Right.length > 0 &&
                    Right.map((item, index) => {
                      return (
                        <li
                          style={{ top: index * 37 }}
                          key={item.id}
                          onClick={() => this.ChangeId(item.id)}
                        >
                          <p>{item.title}</p>
                          <span>{moment(item.createAt).startOf("天").fromNow()}</span>
                        </li>
                      );
                    })}
                </QueueAnim>
              </ul>
            }
          </div>
          <div className="file-r-b">
            {/* 右半边 */}
            <Anchor>
              <h4>目录</h4>
              <h3>欢迎使用 Wipi Markdown 编辑器</h3>

              {data &&
                data.length > 0 &&
                data.map((item, indexs) => {
                  return (
                    <div>
                      <h4
                        key={indexs}
                        onClick={() => this.CreateIndex(indexs)}
                        style={{ color: `${this.state.ind === indexs ? "red" : ""}` }}
                        className={this.state.ind === indexs ? "bgColor" : ""}
                      >
                        {item.title}
                      </h4>
                    </div>
                  );
                })}
            </Anchor>
          </div>
        </div>
        <>
          <Modal
            title="分享海报"
            visible={this.state.visible}
            onOk={this.handleOk.bind(this)}
            onCancel={this.handleCancel.bind(this)}
            okText="下载"
            cancelText="关闭"
          >
            <div>
              <div className="file-r-b-h">{list.cover ? <img src={list.cover} alt="" /> : ""}</div>
              <div className="file-r-b-m">
                <h2>{list.title}</h2>
                <div>
                  <dl>
                    <dt>
                      <QRCode value={`http://localhost:3000/fileDetail/${list.id}`} />
                    </dt>
                    <dd>
                      <p>扫描二维码可查看当前文章</p>
                      <p>
                        原文分享<span className="red">二组实训项目</span>
                      </p>
                    </dd>
                  </dl>
                </div>
              </div>
            </div>
          </Modal>
        </>
      </div>
    );
  }
}
