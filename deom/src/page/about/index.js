import React, { Component } from "react";
import Actionlist from "../../components/Aboutlist/Aboutlist";
import style from "./index.module.less";
import Back from "../../components/Back/Back";
export default class index extends Component {
  render() {
    return (
      <>
        <div className={style.ConeText}>
          <div className={style.ConeTextWrappers}>
            <div className={style.ConeTextWrapper}>
              <div className={style.Connect}>
                <img
                  src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fup.enterdesk.com%2Fedpic%2Fdc%2Fd2%2Fe3%2Fdcd2e350f4fcae5d336b04756417c6dd.jpg&refer=http%3A%2F%2Fup.enterdesk.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1637221722&t=77a85acead12a2b3258d0268400dc79b"
                  alt="文章封面"
                />
                <h2>这世界只有一种英雄主义，就是在看清了生活的真相之后，依然热爱生活。</h2>
                <h3>最近动态</h3>
                <ul>
                  <li>
                    <strong>·</strong> 新工作新方向适应中
                  </li>
                  <li>
                    <strong>·</strong> 备考中
                  </li>
                  <li>
                    <strong>·</strong> 购房观看中
                  </li>
                </ul>
                <p>停更一段时间~</p>
              </div>
            </div>
          </div>
          <Actionlist />
          <Back></Back>
        </div>
      </>
    );
  }
}
