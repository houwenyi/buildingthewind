import React, { Component } from "react";

import { getAllData } from "@/api";
import style from "./index.module.less";
import ArticleList from "@/components/ArticleComs/ArticleList";
export default class AffterEnd extends Component {
  state = {
    allData: [],
  };
  async componentDidMount() {
    let result = await getAllData();
    this.setState(
      {
        allData: result.data,
      },
      () => {
        const arr = this.state.allData.slice(
          Math.floor(Math.random() * this.state.allData.length),
          Math.floor(Math.random() * this.state.allData.length)
        );
        this.setState({
          allData: arr,
        });
      }
    );
  }

  render() {
    const { allData } = this.state;

    return (
      <div id={style.AffterEnd}>
        {allData.length === 0 ? (
          <p style={{ textAlign: "center" }}>暂无数据</p>
        ) : (
          <ArticleList allData={allData} />
        )}
      </div>
    );
  }
}
