import React, { Component } from "react";
import style from "./index.module.less";

import { getAllData } from "@/api";
import ArticleList from "@/components/ArticleComs/ArticleList";
export default class LettCode extends Component {
  state = {
    allData: [],
  };
  async componentDidMount() {
    let result = await getAllData();
    this.setState(
      {
        allData: result.data,
      },
      () => {
        const arr = this.state.allData.slice(
          Math.floor(Math.random() * this.state.allData.length),
          Math.floor(Math.random() * this.state.allData.length)
        );
        this.setState({
          allData: arr,
        });
      }
    );
  }

  render() {
    const { allData } = this.state;

    return (
      <div id={style.LettCode}>
        {allData.length === 0 ? (
          <p style={{ textAlign: "center" }}>暂无数据</p>
        ) : (
          <ArticleList allData={allData} />
        )}
      </div>
    );
  }
}
