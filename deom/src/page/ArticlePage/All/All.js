import React, { Component } from "react";
import style from "./index.module.less";
import moment from "../../../api/Moment";
import { getAllData } from "@/api";

import { HeartOutlined, EyeOutlined, ShareAltOutlined } from "@ant-design/icons";
export default class All extends Component {
  state = {
    allData: [],
  };
  async componentDidMount() {
    let result = await getAllData();
    this.setState({
      allData: result.data,
    });
  }
  goDetail(id) {
    this.props.history.push(`/home/detail/${id}`);
  }
  render() {
    const { allData } = this.state;
    return (
      <div className={style.All}>
        {allData &&
          allData.map((item) => {
            return (
              <div
                key={item.id}
                className={style.knowMain}
                onClick={() => {
                  this.goDetail(item.id);
                }}
              >
                <div className={style.knowMainLeft}>
                  <h3>
                    {item.title}
                    <span>{moment(item.createAt).startOf("天").fromNow()}</span>
                  </h3>
                  <p>{item.title}</p>
                  <div className={style.knowMain_Icon}>
                    <p>
                      <HeartOutlined />
                      <span>{item.likes}</span>
                    </p>
                    <p>
                      <EyeOutlined />
                      <span>{item.likes}</span>
                    </p>
                    <p>
                      <ShareAltOutlined />
                      <span>{item.likes}</span>
                    </p>
                  </div>
                </div>
                <div className={style.knowMainRight}>
                  <img src={item.cover} alt="" />
                </div>
              </div>
            );
          })}
      </div>
    );
  }
}
