import React, { Component } from "react";
import Connectlist from "../../components/Connectlist/Connectlist";
import style from "./index.module.less";
import Back from "../../components/Back/Back";
export default class index extends Component {
  render() {
    return (
      <div className={style.Boards}>
        <Connectlist />
        <Back />
      </div>
    );
  }
}
