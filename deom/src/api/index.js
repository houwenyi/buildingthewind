import axios from "../utils/httptool";
export const FileList = () => axios.get("/api/article/recommend");

//归档右上数据
export const FileRight = () => axios.get("/api/article/recommend");

//归档右下数据
export const FileLeft = () => axios.get("/api/category");

//归档详情
export const getFileDetail = (params) => axios.post(`/api/article/${params}/views`, { params });

//文章右侧标签数据
export const articlesData = () => axios.get("https://creationapi.shbwyz.com/api/article");

//知识小册右侧数据
export const knowledgeData = () =>
  axios.get("https://creationapi.shbwyz.com/api/article/recommend");

// //所有数据请求
export const getAllData = () =>
  axios.get(
    "https://creationapi.shbwyz.com/api/article/recommend?articleId=a405d7e7-3f95-4b43-a221-d8b16303cdf1"
  );
// //带图片的请求
export const knowImgs = () =>
  axios.get("https://creationapi.shbwyz.com/api/knowledge?page=1&pageSize=12&status=publish");

//评论接口
export const comments = () => axios.get("/api/comment");

export const getArticleComment = (params) =>
  axios.get("/api/comment/host/" + params.hostId, { params }); // 获取置顶文章的评论
// import httpTools from "../utils/httpAxios";
// import { message } from "antd";
// const context = require.context("./modules", true, /\.js$/);
// const httpAxios = httpTools({
//   timeout: 1,
//   commonHeaders: {
//     token: window.localStorage.token,
//   },
//   faliMessage: (msg) => {
//     message.error(msg);
//   },
// });
// let pathObj = context.keys().reduce((prev, path) => {
//   prev = { ...prev, ...context(path).default }; // {} Object.assign
//   return prev;
// }, {});

// let res = Object.keys(pathObj).reduce((prev, cur) => {
//   prev[cur] = (data) => {
//     //axios({url,method,data})
//     return httpAxios({
//       ...pathObj[cur],
//       [pathObj[cur].method === "get" ? "params" : "data"]: data,
//     });
//   };
//   return prev;
// }, {});
// export default res;
