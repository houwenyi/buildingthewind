import axios from "axios";
const createRequest = () => {
  const httpTool = axios.create({
    baseURL: "",
    timeout: 10000,
  });
  httpTool.interceptors.request.use(
    (config) => {
      return {
        ...config,
        headers: {
          ...config.headers,
          token: localStorage.getItem("token"),
        },
      };
      // 之前执行
      //open(method,url)
      //send(body)
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  //请求结果拦截
  httpTool.interceptors.response.use(
    (res) => {
      // status === 200
      if (res.data.code === "FAIL") {
        alert(res.data.msg);
        return Promise.reject(res.data);
      }
      return res.data;
    },
    (error) => {
      // status !== 200
      // console.dir(error);
      if (error.code === "ECONNABORTED") {
        alert("您的网络超时，请刷新重试");
        return Promise.reject(error);
      }

      return Promise.reject(error);
    }
  );
  return httpTool;
};
export default createRequest();
