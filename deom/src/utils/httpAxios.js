import axios from "axios";
const httpTools = ({ timeout = 10, commonHeaders = {}, faliMessage }) => {
  const httpAxios = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    timeout: timeout * 1000,
    faliMessage: (msg) => {
      alert(msg);
    },
  });
  //请求拦截 send()之前
  httpAxios.interceptors.request.use(
    (config) => {
      return {
        ...config,
        headers: {
          ...config.headers,
          ...commonHeaders,
        },
      };
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  //响应拦截
  httpAxios.interceptors.response.use(
    (resopnse) => {
      if (resopnse.data.statusCode === 200) {
        return resopnse.data;
      } else {
        return resopnse;
      }
    },
    (error) => {
      if (error.code === "ECONNABORTED") {
        faliMessage("网络超时，请刷新重试!");
      }
      return Promise.reject(error);
    }
  );
  return httpAxios;
};

export default httpTools;
