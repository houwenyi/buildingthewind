const Mock = require("mockjs");
const Data = Mock.mock({
  "list|15": [
    {
      title: "@ctitle",
      img: "@image(120X80,@color)",
      cont: "@cword(20,50)",
      id: "@id",
    },
  ],
});
Mock.mock("/list", Data.list);
