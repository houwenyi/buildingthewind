import { Switch, Route, Redirect } from "react-router-dom";
const RouterView = ({ routerList }) => {
  if (!routerList) {
    return null;
  }
  let list = routerList.filter((item) => item.component);
  let redirect = routerList.filter((item) => !item.component);
  return (
    <Switch>
      {list.map((item, index) => {
        return (
          <Route
            key={index}
            path={item.path}
            render={(pop) => {
              if (item.children) {
                return <item.component {...pop} routerList={item.children} />;
              }
              return <item.component {...pop} />;
            }}
          />
        );
      })}
      {redirect.map((item, index) => {
        return <Redirect key={index} from={item.from} to={item.to} exact />;
      })}
    </Switch>
  );
};

export default RouterView;
