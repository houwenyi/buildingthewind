import loadable from "react-loadable";
const loading = () => {
  return <h1>加载中...</h1>;
};
const Home = loadable({ loader: () => import("../page/Home"), loading: loading });
const ErrorPage = loadable({ loader: () => import("../page/ErrorPage"), loading: loading });
const Articles = loadable({ loader: () => import("../page/articles/index"), loading: loading });
const Archives = loadable({ loader: () => import("../page/archives"), loading: loading });
const Knowledge = loadable({ loader: () => import("../page/knowledge"), loading: loading });
const Boards = loadable({ loader: () => import("../page/boards"), loading: loading });
const About = loadable({ loader: () => import("../page/about"), loading: loading });
const Details = loadable({
  loader: () => import("../page/detail/detail"),
  loading: loading,
});

const All = loadable({ loader: () => import("../page/ArticlePage/All"), loading: loading });
const FrontEnd = loadable({
  loader: () => import("../page/ArticlePage/FrontEnd"),
  loading: loading,
});
const AffterEnd = loadable({
  loader: () => import("../page/ArticlePage/AffterEnd"),
  loading: loading,
});
const Read = loadable({ loader: () => import("../page/ArticlePage/Read"), loading: loading });
const Linux = loadable({ loader: () => import("../page/ArticlePage/Linux"), loading: loading });
const LettCode = loadable({
  loader: () => import("../page/ArticlePage/LettCode"),
  loading: loading,
});
const HightLights = loadable({
  loader: () => import("../page/ArticlePage/HightLights"),
  loading: loading,
});
const routerList = [
  {
    path: "/home",
    component: Home,
    children: [
      {
        path: "/home/articles",
        component: Articles,
        children: [
          {
            path: "/home/articles/all",
            component: All,
          },
          {
            path: "/home/articles/frontend",
            component: FrontEnd,
          },
          {
            path: "/home/articles/affterend",
            component: AffterEnd,
          },
          {
            path: "/home/articles/read",
            component: Read,
          },
          {
            path: "/home/articles/linux",
            component: Linux,
          },
          {
            path: "/home/articles/lettcode",
            component: LettCode,
          },
          {
            path: "/home/articles/hightlights",
            component: HightLights,
          },
        ],
      },

      {
        path: "/home/archives",
        component: Archives,
      },
      {
        path: "/home/knowledge",
        component: Knowledge,
      },
      {
        path: "/home/boards",
        component: Boards,
      },
      {
        path: "/home/about",
        component: About,
      },
      {
        path: "/home/Detail/:id?",
        component: Details,
      },
    ],
  },
  {
    path: "/errorpage",
    component: ErrorPage,
  },
  {
    from: "/",
    to: "/home/articles/all",
  },
  {
    from: "/home/articles",
    to: "/home/articles/all",
  },
];
export default routerList;
