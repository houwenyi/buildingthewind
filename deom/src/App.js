import React, { Component } from "react";

import { IntlProvider } from "react-intl";
import RouterView from "./router/RouterView";
import routerList from "./router/routerList";

import { inject, observer } from "mobx-react";

@inject("titleStore")
@observer
class App extends Component {
  render() {
    let { message, locale } = this.props.titleStore;
    return (
      <IntlProvider messages={message} locale={locale}>
        <RouterView routerList={routerList} />
      </IntlProvider>
    );
  }
}

export default App;
