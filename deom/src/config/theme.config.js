export const light = {
  "@bg-color": "#fff",
  "@text-color": "#000",
};
export const dark = {
  "@bg-color": "#282c35",
  "@text-color": "#aaa",
};
