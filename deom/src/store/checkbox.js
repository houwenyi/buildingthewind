let context = require.context("@/store/mobx", false, /\.js$/);
console.dir(context.keys());
let data = context.keys().reduce((prev, cur) => {
  console.log(cur);
  let key = cur.match(/^\.\/(\w+)\.js$/)[1];
  let val = context(cur).default;
  prev[key] = val;
  return prev;
}, {});

export default data;
